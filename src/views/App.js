import logo from './logo.svg';
import MyComponent from './Example/MyComponent';
import './App.scss';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hello Word
        </p>


        <MyComponent />
      </header>
    </div>
  );
}

export default App;
