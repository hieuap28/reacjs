import React, { Fragment } from "react";

// class ChildComponent extends React.Component {

//     render() {

//         console.log('>>>>> check props:', this.props);

//         let { name, age, address, arrayJobs } = this.props;

//         return (
//             <>
//                 <div className="job-lists">
//                     {
//                         arrayJobs.map((item, index) => {
//                             return (
//                                 <div key={item.id}>
//                                     {item.title} - {item.salary}
//                                 </div>
//                             )
//                         })
//                     }
//                 </div>
//             </>
//         )
//     }
// }

const ChildComponent = (props) => {
    console.log(props);

    let { id, arrayJobs } = props;
    return (
        <>
            <div>
                {
                    arrayJobs.map((item, id) => {
                        if (item.salary >= 500) {
                            return (
                                <div key={id}>
                                    {item.title} - {item.salary} $
                                </div>
                            )
                        }

                    })
                }
            </div>
        </>
    )
}

export default ChildComponent;


